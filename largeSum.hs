import System.IO
main :: IO ()
main = do 
 contents <- readFile "digits"
 let digits = lines contents

 print $ sum (map read digits :: [Integer])


