{-
findCycle :: String -> String -> String
findCycle cycle st
 | length st == 0 || head st == 'e' = ""
 | (cycle == take (length cycle) st) && length cycle > 0 = cycle
 | otherwise = findCycle (cycle ++ [head st]) (tail st) 


longestCycle :: String -> String -> String
longestCycle lc str 
 | length str == 0 = lc
 | (length $ findCycle "" str) > (length lc) = longestCycle (findCycle "" str) (tail str)
 | otherwise = longestCycle lc (tail str)


reciprocalCycle = maximum $ [(length $ longestCycle "" $ drop 2 $ show (1/x), x) | x <- [1..1000]]

-}
findCycle x y ls
 | x == 0 = ls
 | (10*x `mod` y) `elem` ls = ls
 | otherwise = findCycle (x*10 `mod` y) y ((x*10 `mod` y):ls)


longestCycle = snd $ maximum [ (length $ findCycle 1 x [], x) | x <- [1..1000]]
