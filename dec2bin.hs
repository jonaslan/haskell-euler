-- decimal to binary conversion
decToBin :: Int -> [Int]
decToBin x = reverse $ decToBin' x
  where
    decToBin' 0 = []
    decToBin' y = let (a,b) = quotRem y 2 in [b] ++ decToBin' a

-- integer list to string
intListToStr xs = concat $ [show x | x <- xs]

palindrome str = str == reverse str

palList n = [x | x <- [1..n], ( palindrome $ show x ) && ( palindrome $ intListToStr $ decToBin x ) ]

divisors x = [ y | y <- [2..m], x `rem` y == 0]
  where m = x `div` 2

isAbundant x = (x < (sum $ divisors x))

nonAbundantSum x = not $ any isAbundant $ map (x-) [y | y <- [1..m], isAbundant y]
  where m = x `div` 2

allNonAbundantSums n = [x | x <- [1..n], nonAbundantSum x]



