import Data.List
import Data.Maybe (listToMaybe)

primes :: [Integer]
primes = sieve[2..]
 where
  sieve (p:xs) = p : sieve [x | x <- xs, x `mod` p > 0] 
 
primeBs = takeWhile (<1000) primes ++ (map (*(-1)) $ takeWhile (<1000) primes)

factors n = unfoldr (\(d,n) -> listToMaybe [(x, (x, div n x)) | n > 1, 
                           x <- [d..isqrt n] ++ [n], rem n x == 0]) (2,n)
--

 
isPrime n = n > 1 && factors n == [n]
isqrt n = floor . sqrt . fromIntegral $ n

--

quadraticFormula a b n = (n^2 + a*n + b, a, b)

qFs = [ map (quadraticFormula a b) [0..999] | a <- [-999,-997..999], b <- primeBs ]

--qFs = [ (map (quadraticFormula a b) [0..999], a, b) | a <- [-999,-997..999], b <- primeBs]

--qFs' = [ map f [0..1000] | f <- qFs ]

--primeSeries = map (takeWhile isPrime) qFs


{-
primeSeries = map (takeWhile (isPrime)) qFs'

maxPrimeSeries = maximum $ map length primeSeries

index = findIndex (==maxPrimeSeries) (map length primeSeries)
-}

first (x,_,_) = x
second (_,y,_) = y
third (_,_,z) = z


